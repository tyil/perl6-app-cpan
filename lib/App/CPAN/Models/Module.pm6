#! /usr/bin/env false

use v6.d;

use LibUUID;
use Terminal::ANSIColor;

use App::CPAN::Grammars::ModuleString;
use App::CPAN::Model;
use App::CPAN::Subsets;

unit class App::CPAN::Models::Module is App::CPAN::Model;

#| The universally unique ID to refer to this particular module.
has UUID $.uuid;

#| The name of the module.
has Str $.name is required;

#| The auth of the module, if any.
has Str $.auth;

#| The version number of the module.
has Version $.version;

#| The API number of the module, if any.
has Version $.api;

#| The date this particular module was imported into the database.
has DateTime $.imported-on;

#| Create a new Module model with the given arguments.
multi method new (
	Str() :$name!,
	Str() :$auth = "",
	DateTime() :$imported-on,

	# It seems like these fields can't be coerced into the correct types, so
	# additional .new() methods exist to specifically convert each one of these.
	UUID :$uuid,
	Version :$version,
	Version :$api,
) {
	self.bless(
		:$uuid,
		:$name,
		:$auth,
		:$version,
		:$api,
		:$imported-on,
	)
}

# Manual conversion...
multi method new (Str:D :$uuid!, *%params) { samewith(uuid => UUID.new($uuid), |%params) }
multi method new (Str:D :$version!, *%params) { samewith(version => Version.new($version), |%params) }
multi method new (Str:D :$api!, *%params) { samewith(api => Version.new($api), |%params) }

# Aliased fields get renamed to the correct variant here
multi method new (:$imported_on!, *%params) { samewith(imported-on => $imported_on, |%params) }

#| Create a new Module model given a ModuleString.
multi method new (
	ModuleString $input,
) {
	my $result = App::CPAN::Grammars::ModuleString.parse($input);

	# Set up a hash containing the formatted parts of the ModuleString
	my %input = name => ~$result<name>;

	# Include all attributes
	for $result<attributes> -> $attribute {
		%input{~$attribute<name>} = ~$attribute<value>;
	}

	# Create the actual model
	samewith(|%input);
}

method to-hash (
	--> Hash
) {
	%(
		:$!name,
		:$!auth,
		imported-on => ~$!imported-on,
		api => ~$!api,
		uuid => ~$!uuid,
		version => ~$!version,
	)
}

#| Turn the module into a simple string variant.
multi method to-str (
	#| Add coloring to the output. Defaults to False.
	Bool:D :$color where { !$_ } = False,

	--> Str
) {
	my $output = $!name;

	$output ~= ":auth<{$!auth}>" if $!auth;
	$output ~= ":version<{$!version}>";
	$output ~= ":api<{$!api}>" if $!api;

	$output;
}

#| Turn the module into a simple string variant.
multi method to-str (
	#| Add coloring to the output. Defaults to false.
	Bool:D :$color where { $_ } = False,

	--> Str
) {
	sub attribute (
		Str:D $name,
		Str() $value,
		Str:D $color,
		--> Str
	) {
		colored(":", "black")
		~ colored($name, "white")
		~ colored("<", "black")
		~ colored($value, $color)
		~ colored(">", "black")
	}

	my $identifier = colored($!name // "", "bold");

	$identifier ~= attribute("auth", $!auth, "green") if $!auth;
	$identifier ~= attribute("version", $!version, "cyan") if $!version;
	$identifier ~= attribute("api", $!api, "blue") if $!api;

	$identifier;
}

=begin pod

=NAME    App::CPAN::Models::Module
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
