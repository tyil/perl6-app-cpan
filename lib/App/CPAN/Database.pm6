#! /usr/bin/env false

use v6.d;

use DB::Pg;

unit module App::CPAN::Database;

my $connection;

#| Get a usable database handle to execute queries with.
our sub connection ()
{
	return $connection if $connection;

	my $host = %*ENV<DB_HOST> // "localhost";
	my $port = %*ENV<DB_PORT> // 5432;
	my $user = %*ENV<DB_USER> // %*ENV<USER>;
	my $database = %*ENV<DB_NAME> // "cpan";
	my $password = %*ENV<DB_PASSWORD>;

	$connection = DB::Pg.new(conninfo => "host=$host port=$port dbname=$database user=$user password=$password");
}

=begin pod

=NAME    App::CPAN::Database
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

Database connection handling.

=head1 Description

This module contains subroutines to deal with database connections. These are
to be used by the database repository modules to actually interact with the
database.

=end pod

# vim: ft=perl6 noet
