#! /usr/bin/env false

use v6.d;

use Cro::HTTP::Router;
use JSON::Fast;

use App::CPAN::API::V1::Modules;
use App::CPAN::API::V1::PrometheusExporter;

unit module App::CPAN::API::Routes;

sub routes () is export
{
	route {
		include <v0 metrics> => App::CPAN::API::V1::PrometheusExporter::routes;
		include <v0 modules> => App::CPAN::API::V1::Modules::routes;

		get -> {
			# Define a list of attributes we're willing to share
			my @attributes = <
				authors
				description
				license
				name
				perl
				source-url
				version
			>;

			# Grab those attributes from META6.json
			my %meta = $*PROGRAM.parent(2).add("META6.json").slurp.&from-json.grep(*.key ∈ @attributes);

			# Serve the meta-information
			content 'application/json', %meta;
		}
	}
}

=begin pod

=NAME    App::CPAN::API::Routes
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
