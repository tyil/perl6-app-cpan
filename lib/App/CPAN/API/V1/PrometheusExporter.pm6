#! /usr/bin/env false

use v6.d;

use Cro::HTTP::Router;
use Template::Prometheus;
use Template::Prometheus::Metrics::Gauge;

use App::CPAN::Repositories::Module;

unit module App::CPAN::API::V1::PrometheusExporter;

constant ModuleRepo = App::CPAN::Repositories::Module;
constant GaugeMetric = Template::Prometheus::Metrics::Gauge;

our sub routes ()
{
	route {
		get -> {
			content "text/plain", Template::Prometheus
				.new(:prefix<cpan6>)
				.add-metric(GaugeMetric.new(
					name => "distribution_count",
					value => ModuleRepo.count,
					description => "Number of distributions in the database",
				))
				.add-metric(GaugeMetric.new(
					name => "module_count",
					value => ModuleRepo.count-unique,
					description => "Number of unique modules in the database",
				))
				.add-metric(GaugeMetric.new(
					name => "pause_id_count",
					value => ModuleRepo.count-pause-ids,
					description => "Number of PAUSE IDs in the database",
				))
				.Str
		}
	}
}

=begin pod

=NAME    App::CPAN::API::V1::PrometheusExporter
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
