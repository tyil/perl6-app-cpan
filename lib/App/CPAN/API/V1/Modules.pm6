#! /usr/bin/env false

use v6.d;

use Cro::HTTP::Router;

use App::CPAN::Repositories::Module;
use App::CPAN::Subsets;
use App::CPAN::Util;

unit module App::CPAN::API::V1::Modules;

constant ModuleRepo = App::CPAN::Repositories::Module;

our sub routes ()
{
	route {

		#
		# searchModule
		#

		get ->
			:%params where { too-big-since($_) },
			:$accept is header,
		{
			bad-request |formatted-api-error($accept, qq:to/EOF/)
				The "since" value can't be more than 60 days in the past. The
				minimum value is thus
				"{DateTime.now.earlier(days => 60).utc.Str}".
				EOF
		}

		get ->
			:%params where { since-in-future($_) },
			:$accept is header,
		{
			bad-request |formatted-api-error($accept, qq:to/EOF/)
				The "since" value can't be set in the future. The maximum value
				is thus "{DateTime.now.utc.Str}".
				EOF
		}

		get ->
			:%params where { negative-skip($_) },
			:$accept is header,
		{
			bad-request |formatted-api-error($accept, qq:to/EOF/)
				The "skip" value must be a positive integer, including 0.
				EOF
		}

		get ->
			:%params,
			MimeTypeJSON :$accept! is header,
		{
			content $accept, {
				error => False,
				total => ModuleRepo.count(|format-params(%params)),
				results => ModuleRepo
					.get(|format-params(%params))
					.map(*.to-hash)
					.List,
			}
		}

		get -> :%params {
			content "text/plain", ModuleRepo
				.get(|format-params(%params))
				.map(*.to-str)
				.sort
				.join("\n")
		}

		#
		# getModule
		#

		get ->
			Str $name,
			:%params where { ModuleRepo.count(:$name, |%params) > 1 },
			MimeTypeJSON :$accept! is header,
		{
			bad-request |formatted-api-error($accept, q:to/EOF/)
				When looking up a module by a query, it must be globally
				unique. Either use a more strict query, or use UUIDs to find
				modules.
				EOF
		}

		get ->
			Str $name,
			:%params where { ModuleRepo.count(:$name, |%params) == 1 },
			MimeTypeJSON :$accept! is header,
		{
			content $accept, {
				error => False,
				module => ModuleRepo
					.get(:$name, |%params)
					.first
					.to-hash,
			}
		}

		get ->
			UUIDv4 $uuid where { ModuleRepo.has-by-uuid($_) },
			MimeTypeJSON :$accept! is header,
		{
			content $accept, {
				error => False,
				module => ModuleRepo
					.get-by-uuid($uuid)
					.to-hash,
			}
		}
	}
}

#| Format the query params into parameters that can be used in the backend
#| code.
sub format-params (
	%params is copy,
	--> Hash
) {
	%params<since> = DateTime.new($_) with %params<since>;
	%params<skip> = +$_ with %params<skip>;
	%params<name> = $_.subst("*", "%", :g) with %params<name>;
	%params<auth> = $_.subst("*", "%", :g) with %params<auth>;
	%params<version> = $_.subst("*", "%", :g) with %params<version>;
	%params<api> = $_.subst("*", "%", :g) with %params<api>;

	%params;
}

sub negative-skip (
	%params,
	--> Bool
) {
	return False unless %params<skip>:exists;

	%params<skip> < 0;
}

#| Check whether the "since" value is a date in the future.
sub since-in-future (
	%params,
	--> Bool
) {
	return False unless %params<since>:exists;

	DateTime.now < DateTime.new(%params<since>);
}

#| Check whether the "since" value encompasses too many days.
sub too-big-since (
	%params,
	--> Bool
) {
	return False unless %params<since>:exists;

	DateTime.now.earlier(days => 60) > DateTime.new(%params<since>);
}

=begin pod

=NAME    App::CPAN::API::V1::Modules
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
