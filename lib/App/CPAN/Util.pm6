#! /usr/bin/env false

use v6.d;

use Cro::HTTP::Router;
use File::Temp;
use META6;
use String::Fold;
use Terminal::ANSIColor;

use App::CPAN::Subsets;
use App::CPAN::X::MissingMetaJson;

unit module App::CPAN::Util;

#| Format an error thrown by an API endpoint as plaintext.
multi sub formatted-api-error (
	#| Any mimetype.
	$mimetype,

	#| The error message to send.
	Str:D $message,

	--> List
) is export {
	(
		"text/plain",
		$message.&fold,
	)
}

#| Format an error thrown by an API endpoint for use as JSON.
multi sub formatted-api-error (
	#| A mimetype that resembles anything JSON.
	MimeTypeJSON $mimetype,

	#| The error message to send.
	Str:D $message,

	--> List
) is export {
	(
		$mimetype,
		{
			error => True,
			message => $message.lines.join(" "),
		}
	)
}

#| Stringify and colorize a META6 object.
sub colorize-str (
	#| The META6 object to create a colorized, human readable form of.
	META6:D $meta,

	--> Str
) is export {
	sub attribute (
		Str:D $name,
		Str() $value,
		Str:D $color,
		--> Str
	) {
		colored(":", "black")
		~ colored($name, "white")
		~ colored("<", "black")
		~ colored($value, $color)
		~ colored(">", "black")
	}

	my $identifier = colored($meta.name // "", "bold");

	$identifier ~= attribute("auth", $meta.auth, "green") if $meta.auth;
	$identifier ~= attribute("version", $meta.version, "cyan") if $meta.version;
	$identifier ~= attribute("api", $meta.api, "blue") if $meta.api;

	$identifier;
}

#| Extract the META6.json file from a module distribution.
sub extract-meta-from-dist (
	#| The IO::Path object to a dist.
	IO() $dist,

	--> IO::Path
) is export {
	my $tempdir = tempdir;

	note "# Extracting META6.json from $dist.absolute()" if %*ENV<DEBUG>;

	my @contents = (run « tar tf "$dist.absolute()" », :out).out.slurp.lines;
	my $meta6 = @contents.grep(*.ends-with("META6.json")).first;

	App::CPAN::X::MissingMetaJson.new($dist).throw unless $meta6;

	my @tar-args = «
		tar xf "$dist.absolute()"
		--strip 1
		-C "$tempdir.IO.absolute()"
		"$meta6"
	»;

	note "#> @tar-args.join(" ")" if %*ENV<DEBUG>;

	my $tar = run @tar-args;

	die "tar exited with code $tar.exitcode()" unless $tar.exitcode == 0;

	$tempdir.IO.add("META6.json");
}

#| Generate fancy output for an exception.
sub fancy-exception (
	#| The exception to make a note of.
	Exception:D $exception,

	#| Additional information to add, can be used for debugging purposes.
	Str $context,
) is export {
	my $message = colored($exception.^name, "bold red") ~ ": $exception.message()";

	$message ~= " ($context)" if $context;

	note $message;
	note $exception.backtrace.Str if %*ENV<DEBUG> || %*ENV<DEBUG_BACKTRACE>;
}

#| Split the author field into seperate fields.
#|
#| TODO: Actually make this split the info into seperate fields.
sub split-author-field (
	#| The string containing the author string.
	Str:D $author

	--> Hash
) is export {
	my $name = "";
	my $email = "";
	my $website = "";

	given $author.trim {
		when / ^ (.+?) \s* "<" (.+) ">" \s* "(" (.+) ")" $ / {
			$name = ~$0;
			$email = ~$1;
			$website = ~$2;
		}

		when / ^ (.+?) \s* "<" (.+) ">" $ / {
			$name = ~$0;
			$email = ~$1;
		}

		when / ^ (.+?) \s* "(" (.+) ")" $ / {
			$name = ~$0;
			$website = ~$1;
		}

		default {
			$name = $author;
		}
	}

	{ :$name, :$email, :$website }
}

=begin pod

=NAME    App::CPAN::Util
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
