#! /usr/bin/env false

use v6.d;

use App::CPAN::Util;
use META6;
use Terminal::ANSIColor;

unit class App::CPAN::X::MissingMetaAttribute is Exception;

has META6 $.meta;
has Str $.attribute;

multi method new (META6:D $meta, Str:D $attribute) { self.bless(:$meta, :$attribute) }

method message (
	--> Str
) {
	"$!meta.&colorize-str() has no {colored($!attribute, "bold")}!"
}

=begin pod

=NAME    App::CPAN::X::MissingLicense
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
