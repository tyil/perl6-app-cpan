#! /usr/bin/env false

use v6.d;

use App::CPAN::Util;
use META6;

unit class App::CPAN::X::DuplicateModule is Exception;

has META6 $.meta;

multi method new (META6:D $meta) { self.bless(:$meta) }

method message (
	--> Str
) {
	"{$!meta.&colorize-str} already exists!"
}

=begin pod

=NAME    App::CPAN::X::DuplicateModule
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
