#! /usr/bin/env false

use v6.d;

unit class App::CPAN::X::MissingMetaJson is Exception;

has Str $.path;

multi method new (Str:D $dist) { self.bless(:path($dist)) }
multi method new (IO::Path:D $dist) { self.bless(:path($dist.absolute)) }

method message (
	--> Str
) {
	"$!path contains no META6.json"
}

=begin pod

=NAME    App::CPAN::X::MissingMetaJson
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
