#! /usr/bin/env false

use v6.d;

use XML;
use XML::Element;

unit module App::CPAN::Feed;

#| Parse the XML feed as a string to a Perl 6 native structure.
multi sub parse-cpan-feed (
	#| The XML feed, as a Str.
	Str:D $content,

	--> Positional
) is export {
	$content.&from-xml.&transform-xml-to-native;
}

#| Parse the XML feed from a given file to a Perl 6 native structure.
multi sub parse-cpan-feed (
	#| The IO::Path representing the XML feed.
	IO::Path:D $file,

	--> Positional
) is export {
	$file.&from-xml-stream.&transform-xml-to-native;
}

#| Parse a page of the perl.cpan.uploads NNTP board.
multi sub parse-nntp-feed (
	#| The Stringified DOM of the NNTP page you want to have the results of.
	Str:D $dom,

	#| Include Perl 5results.
	Bool:D :$perl5 = True,

	#| Include Perl 6 results.
	Bool:D :$perl6 = True,

	#| Don't include results older than this particular entry.
	:%after,

	--> Positional
) is export {
	use DOM::Tiny;

	my @anchors = DOM::Tiny.parse($dom).find("a");

	@anchors .= grep(*.text.ends-with(".tar.gz"));

	my @items;
	my @new-checks = < title version author perl >;

	ANCHOR:
	for @anchors {
		# Apply some preparation to more easily construct the item hash
		my @parts = .text.words.tail.split("/");
		my @package-parts = @parts.tail.IO.extension("", :parts(2)).Str.split("-");
		my $perl = @parts[3] eq "Perl6" ?? 6 !! 5;

		# Construct the item
		my %item = %(
			title => @package-parts.head(*-1).join("-"),
			version => @package-parts.tail,
			link => '/authors/id/' ~ .text.split(":", 2).tail.trim,
			author => @parts[2],
			perl => $perl,
		);

		note "Found %item<title> (%item<version>)" if %*ENV<DEBUG>;

		# Check if this item is the indicator from where to start collecting, if needed
		if (%after) {
			my Int $passed-checks = 0;

			for @new-checks -> $check {
				$passed-checks++ if %item{$check} eq %after{$check};
			}

			note "Stopped at %item<title> (%item<version>)" if %*ENV<DEBUG> && $passed-checks ≥ @new-checks.elems;

			# All modules that come after this are considered old
			last ANCHOR if $passed-checks ≥ @new-checks.elems;
		}

		# Filter to only the Perl versions requested
		next if $perl == 5 && !$perl5;
		next if $perl == 6 && !$perl6;

		# Add the item to the list
		@items.push: %item;
	}

	@items;
}

#| Transform an XML::Document created from the recent feed on CPAN into a seq
#| containing all the relevant information of the itmes.
sub transform-xml-to-native (
	#| The XML document, created from the recent feed from CPAN.
	XML::Document:D $document,

	--> Positional
) {
	my @items;

	for $document.nodes.grep(* ~~ XML::Element).grep(*.name eq "item") {
		@items.push: %(
			title => .&tag-contents("title"),
			link => .&tag-contents("link"),
			description => .&tag-contents("description"),
			author => .&tag-contents("dc:creator"),
			date => DateTime.new(.&tag-contents("dc:date")),
		)
	}

	@items;
}

#| Retrieve the text content of a given node by its name. When multiple nodes
#| with the same name exist within the given XML::Element, the first one will be
#| used.
sub tag-contents (
	#| The element to look into for children with a given name.
	XML::Element:D $element,

	#| The name to look for.
	Str:D $name,

	--> Str
) {
	my @tags = $element.getElementsByTagName($name);

	return "" unless @tags.elems;

	@tags.first.contents.Str;
}


=begin pod

=NAME    App::CPAN::Feed
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
