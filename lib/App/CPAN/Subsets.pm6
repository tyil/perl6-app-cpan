#! /usr/bin/env false

use v6.d;

use App::CPAN::Grammars::ModuleString;

unit module App::CPAN::Subsets;

subset MimeTypeJSON of Str is export where * eq "application/json";
subset ModuleString of Str is export where { App::CPAN::Grammars::ModuleString.parse($_) };
subset UUIDv4 of Str is export where / ^ <.xdigit> ** 8 [ "-" <.xdigit> ** 4 ] ** 3 "-" <.xdigit> ** 12 $ /;

=begin pod

=NAME    App::CPAN::Subsets
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
