#! /usr/bin/env false

use v6.d;

unit role App::CPAN::Model;

#| Turn the model into a human-readable stringified form.
method Str (
	--> Str
) {
	self.to-str
}

method to-hash (--> Hash) { * }
method to-str  (--> Str ) { * }

=begin pod

=NAME    App::CPAN::Model
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
