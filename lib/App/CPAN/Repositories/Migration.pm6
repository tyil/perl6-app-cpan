#! /usr/bin/env false

use v6.d;

use App::CPAN::Database;

unit class App::CPAN::Repositories::Migration;

my IO::Path $basedir = $*PROGRAM.parent(2).add("sql/Pg");

#| Retrieve a list of all known migrations that have been run.
method all (
	--> Iterable
) {
	my $dbh = App::CPAN::Database::connection.db;
	my $stmt = $dbh.prepare(q:to/STMT/);
		SELECT name FROM _migrations ORDER BY name;
		STMT

	my $results = $stmt.execute;

	$dbh.finish;
	$results.hashes;
}

#| Retrieve the migration which was ran last.
method last (
	--> Associative
) {
	my $dbh = App::CPAN::Database::connection.db;
	my $stmt = $dbh.prepare(q:to/STMT/);
		SELECT name FROM _migrations ORDER BY name DESC LIMIT 1;
		STMT

	my $results = $stmt.execute;

	$dbh.finish;
	$results.hash;
}

#| Run a given upgrade migration by (file)name.
method up (
	#| The name of the migrations
	Str:D $name,

	--> Str
) {
	my $migration = $basedir.add("up").add($name);
	my $dbc = App::CPAN::Database::connection;

	die "No such upgrade migration: $migration.absolute()" unless $migration.f;

	$dbc.execute($migration.slurp);

	$name;
}

#| Run a given downgrade migration by (file)name.
method down (
	#| The name of the migration.
	Str:D $name,

	--> Str
) {
	my $migration = $basedir.add("down").add($name);
	my $dbc = App::CPAN::Database::connection;

	die "No such downgrade migration: $migration.absolute()" unless $migration.f;

	$dbc.execute($migration.slurp);

	$name;
}

=begin pod

=NAME    App::CPAN::Repositories::Migration
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
