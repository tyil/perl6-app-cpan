#! /usr/bin/env false

use v6.d;

use App::CPAN::Database;
use LibUUID;

use App::CPAN::Models::Module;
use App::CPAN::Subsets;

unit class App::CPAN::Repositories::Module;

constant ModuleModel = App::CPAN::Models::Module;

#| Add a new module to the database.
method add (
	#| The :api value of the module.
	Str() :$api = "",

	#| The :auth value of the module.
	Str() :$auth = "",

	#| The authors of the module.
	:@authors = [],

	#| The PAUSE ID of the user that uploaded it to CPAN.
	Str() :$pause-id = "",

	#| The dependencies of the module.
	:@dependencies = [],

	#| The short description of the module.
	Str() :$description,

	#| The license of the module.
	Str() :$license,

	#| The name of the module.
	Str() :$name,

	#| The provides of the module.
	:%provides = {},

	#| The resources of the module.
	:@resources = [],

	#| The source-url of the module.
	Str() :$source-url,

	#| The tags of the module.
	:@tags = [],

	#| The version string of the module.
	Str() :$version,

	--> UUID
) {
	my $dbh = App::CPAN::Database::connection.db;

	try {
		CATCH { default { $dbh.finish; $_.throw; } }

		$dbh.begin;

		# Add the main module record
		my $stmt = $dbh.prepare(q:to/STMT/);
			INSERT INTO modules (pause_id, name, version, api, auth, license, description, source_url)
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
			RETURNING uuid
			STMT

		my $results = $stmt.execute($pause-id, $name, $version, $api, $auth, $license, $description, $source-url);
		my $uuid = $results.hash<uuid>;

		note "# Inserted module as $uuid" if %*ENV<DEBUG> || %*ENV<DEBUG_IMPORT>;

		my $author-stmt = $dbh.prepare(q:to/STMT/);
			INSERT INTO module_authors (module_uuid, name, email, website)
			VALUES ($1, $2, $3, $4)
			RETURNING uuid
			STMT

		# Add authors
		for @authors {
			$author-stmt.execute($uuid, $_<name>, $_<email>, $_<website>);

			note "# Inserted author $_<name> as {$author-stmt.row.first}" if %*ENV<DEBUG> || %*ENV<DEBUG_IMPORT>;
		}

		my $depend-stmt = $dbh.prepare(q:to/STMT/);
			INSERT INTO module_dependencies (module_uuid, dependency_string)
			VALUES ($1, $2)
			RETURNING uuid
			STMT

		# Add dependencies
		for @dependencies {
			$depend-stmt.execute($uuid, $_);

			note "# Inserted dependency $_ as {$depend-stmt.row.first}" if %*ENV<DEBUG> || %*ENV<DEBUG_IMPORT>;
		}

		my $provide-stmt = $dbh.prepare(q:to/STMT/);
			INSERT INTO module_provides (module_uuid, module, file)
			VALUES ($1, $2, $3)
			RETURNING uuid
			STMT

		# Add provides
		for %provides.kv -> $module, $file {
			$provide-stmt.execute($uuid, $module, $file);

			note "# Inserted provide $module as {$provide-stmt.row.first}" if %*ENV<DEBUG> || %*ENV<DEBUG_IMPORT>;
		}

		# Add resources
		my $resource-stmt = $dbh.prepare(q:to/STMT/);
			INSERT INTO module_resources (module_uuid, file)
			VALUES ($1, $2)
			RETURNING uuid
			STMT

		for @resources {
			$resource-stmt.execute($uuid, $_);

			note "# Inserted resource $_ as {$resource-stmt.row.first}" if %*ENV<DEBUG> || %*ENV<DEBUG_IMPORT>;
		}

		# Add tags
		my $tag-stmt = $dbh.prepare(q:to/STMT/);
			INSERT INTO module_tags (module_uuid, name)
			VALUES ($1, $2)
			RETURNING uuid
			STMT

		for @tags {
			$tag-stmt.execute($uuid, $_);

			note "# Inserted tag $_ as {$tag-stmt.row.first}" if %*ENV<DEBUG> || %*ENV<DEBUG_IMPORT>;
		}

		$dbh.commit unless %*ENV<DRY_RUN>;
		$dbh.finish;

		$uuid;
	}
}

#| Get the total count of modules currently in the database.
multi method count (
	--> Int
) {
	my $dbh = App::CPAN::Database::connection.db;
	my $stmt = $dbh.prepare(q:to/STMT/);
		SELECT COUNT(*)
		FROM modules
		STMT

	my $result = $stmt.execute;

	$dbh.finish;

	$result.value;
}

#| Count modules that match the query.
multi method count (
	Str() :$name = "%",
	Str() :$version = "%",
	Str() :$api = "%",
	Str() :$auth = "%",
	DateTime :$since,

	--> Int
) {
	my $dbh = App::CPAN::Database::connection.db;
	my $stmt = $dbh.prepare(q:to/STMT/);
		SELECT COUNT(*)
		FROM modules
		WHERE name LIKE $1
		AND version LIKE $2
		AND api LIKE $3
		AND auth ILIKE $4
		AND created_at > $5
		STMT

	my $result = $stmt.execute(
		$name,
		$version,
		$api,
		$auth,
		$since // DateTime.new("1970-01-01T00:00:00Z"),
	);

	$dbh.finish;
	$result.value;
}

#| Count the number of unique modules currently in the database. Uniqueness is
#| determined by the name and the auth of the module.
method count-unique (
	--> Int
) {
	my $dbh = App::CPAN::Database::connection.db;
	my $stmt = $dbh.prepare(q:to/STMT/);
		SELECT COUNT(DISTINCT concat(name, ':auth<', auth, '>'))
		FROM modules
		STMT

	my $result = $stmt.execute;

	$dbh.finish;

	$result.value;
}

#| Get the total number of unique CPAN IDs used in modules.
method count-pause-ids (
	--> Int
) {
	my $dbh = App::CPAN::Database::connection.db;
	my $stmt = $dbh.prepare(q:to/STMT/);
		SELECT COUNT(DISTINCT pause_id)
		FROM modules
		STMT

	my $result = $stmt.execute;

	$dbh.finish;

	$result.value;
}

#| Check if a given module is already available in the database.
method has (
	Str() :$name,
	Str() :$auth,
	Str() :$version,
	Str() :$api,

	--> Bool
) {
	my $dbh = App::CPAN::Database::connection.db;
	my $stmt = $dbh.prepare(q:to/STMT/);
		SELECT count(uuid)
		FROM modules
		WHERE name = $1
		AND auth = $2
		AND version = $3
		AND api = $4
		STMT

	my $result = $stmt.execute($name, $auth, $version, $api);

	$dbh.finish;

	(0 < $result.value);
}

multi method has-by-uuid (
	UUID:D $uuid,

	--> Bool
) {
	my $dbh = App::CPAN::Database::connection.db;
	my $stmt = $dbh.prepare(q:to/STMT/);
		SELECT count(uuid)
		FROM modules
		WHERE uuid = $1
		STMT

	my $result = $stmt.execute(~$uuid);

	$dbh.finish;

	(0 < $result.value);
}

multi method has-by-uuid (
	Str:D $uuid,

	--> Bool
) {
	samewith(UUID.new($uuid))
}

#| Retrieve modules from the database.
method get (
	Str:D :$name = "%",
	Str:D :$auth = "%",
	Str:D :$version = "%",
	Str:D :$api = "%",
	Int:D :$limit = 50,
	Str:D :$order-by = "created_at",
	Int:D :$skip = 0,
	DateTime :$since,
) {
	my $dbh = App::CPAN::Database::connection.db;
	my $stmt = $dbh.prepare(q:to/STMT/);
		SELECT
			uuid,
			name,
			version,
			api,
			auth,
			created_at AS imported_on
		FROM modules
		WHERE created_at > $3
		AND name ILIKE $5
		AND auth ILIKE $6
		AND version ILIKE $7
		AND api ILIKE $8
		ORDER BY $1 ASC
		LIMIT $2
		OFFSET $4
		STMT

	my $result = $stmt.execute(
		$order-by,
		$limit,
		$since // DateTime.new("1970-01-01T00:00:00Z"),
		$skip,
		$name,
		$auth,
		$version,
		$api,
	);

	$dbh.finish;

	$result.hashes.map({ ModuleModel.new(|$_) });
}

#| Retrieve a module by its UUID.
multi method get-by-uuid (
	#| The UUID of the module to fetch.
	UUID:D $uuid,

	--> ModuleModel
) {
	my $dbh = App::CPAN::Database::connection.db;
	my $stmt = $dbh.prepare(q:to/STMT/);
		SELECT uuid, name, version, api, auth, created_at
		FROM modules
		WHERE uuid = $1
		STMT

	my $result = $stmt.execute(~$uuid);

	$dbh.finish;

	my @records = $result.hashes;

	return ModuleModel unless @records.elems == 1;

	ModuleModel.new(|@records.first);
}

multi method get-by-uuid (
	Str:D $uuid,

	--> ModuleModel
) {
	samewith(UUID.new($uuid));
}

=begin pod

=NAME    App::CPAN::Repositories::Module
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
