#! /usr/bin/env false

use v6.d;

use App::CPAN::Database;
use App::CPAN::Repositories::Migration;
use Terminal::ANSIColor;

unit module App::CPAN::Bin::Migrate;

constant MigrationRepo = App::CPAN::Repositories::Migration;

#| Undo the latest migration
multi sub MAIN (
	Bool:D :$down!
) is export {
	CATCH {
		when .message ~~ / "relation \"_migrations\" does not exist" / {
			note "No _migrations left to undo";
			exit 1
		}
	}

	my $last = MigrationRepo.last<name>;

	MigrationRepo.down($last);

	say "Ran downgrade migration {color("bold")}{$last}{color("reset")}";
}

#| List the migrations
multi sub MAIN (
	Bool:D :$list!
) is export {
	my %migrations = $*PROGRAM.parent(2).add("sql/Pg/up").dir.map(*.basename => False);

	try {
		CATCH {
			when .message ~~ / "relation \"_migrations\" does not exist" / {
				# Silently ignore the missing table
			}
		}

		for MigrationRepo.all {
			%migrations{$_<name>}++;
		}
	}

	for %migrations.keys.sort {
		%migrations{$_}
			?? print color("green")
			!! print color("red")
			;

		print $_ ~ color("reset") ~ "\n";
	}
}

#| Run migrations
multi sub MAIN (
	Bool:D :$up!
) is export {
	my @todo;
	my @migrations = $*PROGRAM.parent(2).add("sql/Pg/up").dir.map(*.basename).sort;

	try {
		CATCH {
			when .message ~~ / "relation \"_migrations\" does not exist" / {
				@todo = @migrations;
			}
		}

		my $found = False;
		my $last = MigrationRepo.last;

		for @migrations {
			if ($_ eq $last) {
				$found = True;
				next;
			}

			next unless $found;

			@todo.push: $_;
		}
	}

	if (!@todo) {
		say "No migrations left!";
		return;
	}

	for @todo {
		MigrationRepo.up($_);
		say "Ran upgrade migration {color("bold")}{$_}{color("reset")}";
	}
}


=begin pod

=NAME    App::CPAN::Bin::Migrate
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
