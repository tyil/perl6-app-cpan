#! /usr/bin/env false

use v6.d;

use App::CPAN::Feed;
use App::CPAN::Repositories::Module;
use App::CPAN::Util;
use App::CPAN::X::DuplicateModule;
use App::CPAN::X::MissingMetaAttribute;
use File::Temp;
use HTTP::UserAgent :simple;
use LibUUID;
use META6;
use Terminal::ANSIColor;

unit module App::CPAN::Bin::Import;

constant ModuleRepo = App::CPAN::Repositories::Module;

#| Parse CPAN's file listing. This is the most complete, but also the most
#| resource intensive.
multi sub MAIN (
	Bool:D :$cpan! where { $_ },

	#| Sync local cache before parsing it.
	Bool:D :$sync = True,
) is export {
	%*ENV<XDG_CACHE_HOME> //= %*ENV<HOME> ~ "/.cache";

	my $cache = (%*ENV<XDG_CACHE_HOME> ~ "/cpan/index").IO;

	if ($sync) {
		my $cpan-url = "cpan-rsync.perl.org::CPAN/authors/id";

		$cache.mkdir unless $cache.d;

		run «
			rsync
			--prune-empty-dirs
			--delete
			-av
			"--exclude=/id/P/PS/PSIXDISTS/Perl6"
			"--include=/id/*/*/*/Perl6/"
			"--include=/id/*/*/*/Perl6/*.tar.gz"
			"--exclude=/id/*/*/*/Perl6/*"
			"--exclude=/id/*/*/*/*"
			"--exclude=id/*/*/CHECKSUMS"
			"--exclude=id/*/CHECKSUMS"
			"$cpan-url"
			"$cache.absolute()"
		»;
	}

	sub get-all-dists (IO::Path $dir) {
		my @dists;

		for $dir.dir -> $object {
			if ($object.d) {
				@dists.append: samewith($object);
				next;
			}

			next unless $object.extension(:2parts) eq "tar.gz";

			@dists.push: $object;
		}

		@dists;
	}

	for get-all-dists($cache) -> $dist {
		CATCH { default { .&fancy-exception($dist.basename) } }

		my $meta = META6.new(:file(extract-meta-from-dist($dist)));

		# All Perl 6 modules appear in the Perl 6 directory, which is a direct
		# child of the directory named after the PAUSE ID. Additionally, it
		# seems that subdirectories are not properly scanned, so the second
		# parent of every dist is always the directory named after a PAUSE ID.
		my $pause-id = $dist.parent(2).basename;

		if import-by-meta6($meta, $pause-id) -> $uuid {
			say "$meta.&colorize-str() added as {colored(~$uuid, "bold")}.";
		}
	}
}

#| Import a module through it's distribution tarball.
multi sub MAIN (
	#| Path to the file to import.
	IO() $file,

	Bool:D :$dist! where { $_ },
) is export {
	die "No such file at $file.absolute()." unless $file.f;
	die "Distributions have to end in .tar.gz" unless $file.extension(:2parts) eq "tar.gz";

	my $meta = META6.new(:file(extract-meta-from-dist($file)));

	if import-by-meta6($meta) -> $uuid {
		CATCH { default { .&fancy-exception($dist); next } }

		say "$meta.&colorize-str() added as {colored(~$uuid, "bold")}.";
	}
}

#| Import a module through it's META6.json file.
multi sub MAIN (
	#| Path to the file to import.
	IO() $file,

	Bool:D :$meta6! where { $_ },
) is export {
	my $meta = META6.new(:$file);

	if import-by-meta6($meta) -> $uuid {
		CATCH { default { .&fancy-exception; next } }

		say "$meta.&colorize-str() added as {colored(~$uuid, "bold")}.";
	}
}

#| Import modules from nntp.perl.org.
multi sub MAIN (
	Bool:D :$nntp! where { $_ },
) is export {
	my $ua = HTTP::UserAgent.new;
	my $body = $ua.get("https://www.nntp.perl.org/group/perl.cpan.uploads/").content;
	my $upstream = "https://cpan.metacpan.org";
	my @modules = $body.&parse-nntp-feed(:!perl5);

	for @modules -> %module {
		CATCH {
			when App::CPAN::X::DuplicateModule {
				.&fancy-exception("%module<title>, v%module<version>") if %*ENV<DEBUG> || %*ENV<DEBUG_DUPLICATES>;
				next;
			}
			default { .&fancy-exception("%module<title>, v%module<version>"); next; }
		}

		my $tempdir = tempdir.IO;
		my $dist = $tempdir.add(%module<link>.split("/").tail);
		my $meta-json = $tempdir.add("META6.json");

		# Download the tarball
		note "# Downloading $upstream%module<link>" if %*ENV<DEBUG> || %*ENV<DEBUG_IMPORT>;

		with ($dist.open(:w, :bin, :enc(Nil))) {
			LEAVE { .close }

			.spurt: $ua.get($upstream ~ %module<link>, :bin).content;
		}

		my $meta = META6.new(:file(extract-meta-from-dist($dist)));

		# Import the META6.json
		if import-by-meta6($meta, %module<author>) -> $uuid {
			say "$meta.&colorize-str() added as {colored(~$uuid, "bold")}.";
		}
	}
}

#| Import a module using the information from its META6 object.
sub import-by-meta6 (
	#| The META6 object based on the module's META6.json file.
	META6:D $meta,

	#| The PAUSE ID of the uploader.
	Str:D $pause-id = "",

	--> UUID
) {
	App::CPAN::X::MissingMetaAttribute.new($meta, "license").throw unless $meta.license;
	App::CPAN::X::DuplicateModule.new($meta).throw if ModuleRepo.has(
		:api($meta.api // "")
		:auth($meta.auth // "")
		:name($meta.name)
		:version($meta.version)
	);

	ModuleRepo.add(
		:api($meta.api // "")
		:auth($meta.auth // "")
		:authors($meta.authors.map(*.&split-author-field))
		:dependencies($meta.depends)
		:description($meta.description)
		:license($meta.license)
		:name($meta.name)
		:$pause-id
		:provides($meta.provides)
		:resources($meta.resources)
		:source-url($meta.source-url // "")
		:version($meta.version)
	);
}

=begin pod

=NAME    App::CPAN::Bin::Import
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
