#! /usr/bin/env false

use v6.d;

use Cro::HTTP::Log::File;
use Cro::HTTP::Router;
use Cro::HTTP::Server;

use App::CPAN::API::Routes;

unit module App::CPAN::Bin::API;

#| Run the CPAN JSON API.
sub MAIN (
	#| The host to bind to. Defaults to localhost.
	Str:D :$host = 'localhost',
	
	#| The port to bind to. Defaults to 51859.
	Int:D :$port = 51859,

	#| The base path to the application, if any.
	Str:D :$base-path = %*ENV<CPAN_API_BASE_PATH> // "",
) is export {
	# An empty base-path used with include() would result in requiring
	# //<endpoints> (double slash) being required.
	my $application = $base-path
		?? route { include $base-path => routes }
		!! routes
		;

	# Build the Cro server object.
	my $service = Cro::HTTP::Server.new(
		:$host,
		:$port,
		:$application,
		:after([
			Cro::HTTP::Log::File.new(
				:logs($*OUT),
				:errors($*ERR),
			),
		]),
	);

	# Friendly notice to show where it's running at.
	say "Starting CPAN API on $host:$port/$base-path…";

	# Start Cro
	$service.start;

	# Add a react block to kill stop Cro whenever ^C is entered.
	react {
		whenever signal(SIGTERM) {
			say "Received SIGTERM, shutting down…";
			$service.stop;
			done;
		}
		whenever signal(SIGINT) {
			say "Received SIGINT, shutting down…";
			$service.stop;
			done;
		}
	}
}

=begin pod

=NAME    App::CPAN::Bin::API
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
