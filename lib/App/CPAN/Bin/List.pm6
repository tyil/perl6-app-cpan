#! /usr/bin/env false

use v6.d;

use App::CPAN::Repositories::Module;

unit module App::CPAN::Bin::List;

constant RepoModule = App::CPAN::Repositories::Module;

#| List modules from the database.
sub MAIN (
	#| Enable colored output.
	Bool:D :$color = True,
) is export {
	RepoModule.get.map(*.to-str(:$color)).sort.join("\n").say;
}

=begin pod

=NAME    App::CPAN::Bin::List
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
