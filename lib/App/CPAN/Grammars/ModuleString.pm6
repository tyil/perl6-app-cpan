#! /usr/bin/env false

use v6.d;

unit grammar App::CPAN::Grammars::ModuleString;

token TOP
{
	^
	<name>
	<attributes=attribute>*
	$
}

token name
{
	[\w+]+ % "::"
}

token attribute
{
	":"
	$<name> = [ \w+ ]
	"<"
	$<value> = [ <[\w.+*:]>+ ]
	">"
}

=begin pod

=NAME    App::CPAN::Grammars::ModuleString
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.2

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
