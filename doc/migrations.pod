=head1 Database migrations

=head2 Layout

All the database migrations (upgrades and downgrades of the schema) reside in
the C<sql> subdirectory. In this directory you can find a directory for each
supported database engine, with each of these directories containing an C<up>
for database schema upgrades, and a C<down> for database schema downgrades.

=head2 Naming conventions

=head3 Constraints for foreign key relationships

Each foreign key relationship constraint has a name like
C<{child_table}_{child_column}_{parent_table}_{parent_column}>.

=head3 SQL files

The SQL files are named as C<{App::CPAN release}-{description}.sql>. This naming
scheme is important for the database migration program to work correctly.
