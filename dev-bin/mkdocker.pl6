#! /usr/bin/env perl6

use v6.d;

use JSON::Fast;

#| Build a tagged image, generally used for testing a new release before making
#| it official. If no tag is given, build "latest".
multi sub MAIN (Str:D $tag = "latest")
{
	my IO::Path $basedir = $*PROGRAM.parent(2);
	my %meta = $basedir.add("META6.json").slurp.&from-json;

	run «
		docker build
		-t "{docker-tag(%meta, version => $tag)}"
		"$basedir.absolute()"
	»;
}

#| Build a release image, which has it's tag set depending on the current
#| version specified in META6.json.
multi sub MAIN ("release")
{
	my IO::Path $basedir = $*PROGRAM.parent(2);
	my %meta = $basedir.add("META6.json").slurp.&from-json;

	run «
		docker build
		-t "{docker-tag(%meta)}"
		"$basedir.absolute()"
	»;
}

#| Generate the docker tag.
sub docker-tag (
	#| Hash of META6 info.
	%meta,

	#| Override for the version identifier.
	Str :$version is copy,

	--> Str
) {
	my $tag = "";

	my $name = "cpan";
	$version //= %meta<version>;

	$tag ~= "%*ENV<USER>/" if %*ENV<USER>;
	$tag ~= "$name:$version";

	$tag;
}
