BEGIN;

-- This table holds information on which queries have been applied, and which
-- have not been applied yet. It's not covered in the ERD document, as it is
-- considered an implementation detail.

CREATE TABLE _migrations (
	name VARCHAR
);

-- These tables form the initial database structure that allows one to keep
-- information on the modules.

CREATE TABLE modules (
	uuid UUID PRIMARY KEY DEFAULT gen_random_uuid(),
	pause_id VARCHAR NOT NULL,
	name VARCHAR NOT NULL,
	auth VARCHAR NOT NULL DEFAULT '',
	version VARCHAR NOT NULL DEFAULT '',
	api VARCHAR NOT NULL DEFAULT '',
	license VARCHAR NOT NULL DEFAULT '',
	description VARCHAR NOT NULL DEFAULT '',
	source_url VARCHAR NOT NULL DEFAULT '',
	created_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE module_authors (
	uuid UUID PRIMARY KEY DEFAULT gen_random_uuid(),
	module_uuid UUID NOT NULL,
	name VARCHAR NOT NULL DEFAULT '',
	email VARCHAR NOT NULL DEFAULT '',
	website VARCHAR NOT NULL DEFAULT ''
);

CREATE TABLE module_dependencies (
	uuid UUID PRIMARY KEY DEFAULT gen_random_uuid(),
	module_uuid UUID NOT NULL,
	dependency_string VARCHAR NOT NULL,
	dependency_uuid UUID
);

CREATE TABLE module_provides (
	uuid UUID PRIMARY KEY DEFAULT gen_random_uuid(),
	module_uuid UUID NOT NULL,
	module VARCHAR NOT NULL,
	file VARCHAR NOT NULL
);

CREATE TABLE module_resources (
	uuid UUID PRIMARY KEY DEFAULT gen_random_uuid(),
	module_uuid UUID NOT NULL,
	file VARCHAR NOT NULL
);

CREATE TABLE module_tags (
	uuid UUID PRIMARY KEY DEFAULT gen_random_uuid(),
	module_uuid UUID NOT NULL,
	name VARCHAR NOT NULL
);

-- Add relations between the tables

ALTER TABLE module_authors ADD CONSTRAINT module_authors_module_uuid_module_uuid FOREIGN KEY (module_uuid) REFERENCES modules (uuid);
ALTER TABLE module_dependencies ADD CONSTRAINT module_dependencies_module_uuid_module_uuid FOREIGN KEY (module_uuid) REFERENCES modules (uuid);
ALTER TABLE module_provides ADD CONSTRAINT module_provides_module_uuid_module_uuid FOREIGN KEY (module_uuid) REFERENCES modules (uuid);
ALTER TABLE module_resources ADD CONSTRAINT module_resources_module_uuid_module_uuid FOREIGN KEY (module_uuid) REFERENCES modules (uuid);
ALTER TABLE module_tags ADD CONSTRAINT module_tags_module_uuid_module_uuid FOREIGN KEY (module_uuid) REFERENCES modules (uuid);

-- Write this migration to the migration table

INSERT INTO _migrations (name) VALUES ('0.0.0-create-initial-db.sql');

COMMIT;
