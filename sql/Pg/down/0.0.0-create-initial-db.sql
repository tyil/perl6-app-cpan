BEGIN;

-- Remove relationships

ALTER TABLE module_tags DROP CONSTRAINT module_tags_module_uuid_module_uuid;
ALTER TABLE module_resources DROP CONSTRAINT module_resources_module_uuid_module_uuid;
ALTER TABLE module_provides DROP CONSTRAINT module_provides_module_uuid_module_uuid;
ALTER TABLE module_dependencies DROP CONSTRAINT module_dependencies_module_uuid_module_uuid;
ALTER TABLE module_authors DROP CONSTRAINT module_authors_module_uuid_module_uuid;

-- Remove tables

DROP TABLE module_tags;
DROP TABLE module_resources;
DROP TABLE module_provides;
DROP TABLE module_dependencies;
DROP TABLE module_authors;
DROP TABLE modules;

-- Drop the migrations table

DROP TABLE _migrations;

COMMIT;
