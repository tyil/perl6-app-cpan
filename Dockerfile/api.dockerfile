FROM tyil/perl6:debian-dev-latest AS build

ENV PERL6LIB=/root/lib

RUN apt update && apt install -y libssl-dev

COPY META6.json META6.json

RUN zef install --deps-only --/test .

FROM tyil/perl6:debian-latest

WORKDIR /app

ENV PERL6LIB=/app/lib

RUN apt update && apt install -y libpq-dev libssl-dev

COPY --from=build /usr/local /usr/local
COPY META6.json META6.json
COPY bin bin
COPY lib lib

CMD [ "bash" ]
