FROM swaggerapi/swagger-ui

ENV SWAGGER_JSON=/data/spec.yaml

COPY doc/swagger.yaml /data/spec.yaml
