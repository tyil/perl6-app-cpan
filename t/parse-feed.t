#! /usr/bin/env perl6

use v6.d;

use App::CPAN::Feed;
use Test;

plan 2;

subtest "Read XML into list of packages", {
	my @items = "t/files/recent.xml".IO.slurp.&parse-cpan-feed;

	plan 1 + (@items.elems * 5);

	is @items.elems, 100, "Found 100 items";

	for @items -> %item {
		ok %item<title>:exists, "Found title '%item<title>'";
		ok %item<link>:exists, "Found link '%item<link>'";
		ok %item<description>:exists, "Found description '%item<description>'";
		ok %item<author>:exists, "Found author '%item<author>'";
		ok %item<date>:exists, "Found date '%item<date>'";
	}
}

subtest "Read NNTP page into list of packages", {
	plan 4;

	subtest "#nofilter", {
		my @items = "t/files/recent-nntp.html".IO.slurp.&parse-nntp-feed;

		plan 1 + (@items.elems * 5);

		is @items.elems, 75, "Found 75 items";

		for @items -> %item {
			ok %item<title>:exists, "Title exists for %item<title>";
			ok %item<version>:exists, "Version exists for %item<version>";
			ok %item<link>:exists, "Link exists for %item<title>";
			ok %item<author>:exists, "Author exists for %item<title>";
			ok %item<perl>:exists, "Perl version exists for '%item<title>'";
		}
	}

	subtest ":!perl6", {
		my @items = "t/files/recent-nntp.html".IO.slurp.&parse-nntp-feed(:!perl6);

		plan 1 + @items.elems;

		is @items.elems, 61, "Found 61 items";

		for @items -> %item {
			is %item<perl>, 5, "Perl version for %item<title> is 5";
		}
	}

	subtest ":!perl5", {
		my @items = "t/files/recent-nntp.html".IO.slurp.&parse-nntp-feed(:!perl5);

		plan 1 + @items.elems;

		is @items.elems, 14, "Found 14 items";

		for @items -> %item {
			is %item<perl>, 6, "Perl version for %item<title> is 6";
		}
	}

	subtest "Limit to items that come after a given item", {
		plan 1;

		my %last = %(
			title => "OO-Plugin",
			version => "v0.0.2",
			link => '$CPAN/authors/id/V/VR/VRURG/Perl6/OO-Plugin-v0.0.2.tar.gz',
			author => "VRURG",
			perl => 6,
		);

		my @items = "t/files/recent-nntp.html".IO.slurp.&parse-nntp-feed(:!perl5, :after(%last));

		is @items.elems, 6, "Found 6 items";
	}
}

# vim: ft=perl6 noet
