#! /usr/bin/env perl6

use v6.d;

use Test;

use App::CPAN::Subsets;

plan 6;

my @cases = <
	App
	App::CPAN
	App::CPAN:version<0.1.0>
	App::CPAN:version<0.1.0+>
	App::CPAN:version<0.1.0+>:api<*>
	App::CPAN:version<0.1.0+>:api<*>:auth<cpan:TYIL>
>;

for @cases {
	ok $_ ~~ ModuleString, $_;
}

# vim: ft=perl6 noet
