#! /usr/bin/env perl6

use v6.d;

use LibUUID;
use Test;

use App::CPAN::Models::Module;

constant ModuleModel = App::CPAN::Models::Module;

plan 2;

subtest ".new(|Hash)", {
	plan 5;

	my $module = ModuleModel.new(
		:uuid<6a96fe82-8038-43f2-9c51-591004249e89>,
		:name<App::CPAN>,
		:version<0.1.0>,
		:api<*>,
		:auth<cpan:TYIL>,
	);

	is $module.uuid, "6a96fe82-8038-43f2-9c51-591004249e89", "UUID is correct";
	is $module.name, "App::CPAN", "Name is correct";
	is $module.version, "0.1.0", "Version is correct";
	is $module.api, "*", "API is correct";
	is $module.auth, "cpan:TYIL", "Auth is correct";
}

subtest ".new(ModuleStr)", {
	plan 4;

	subtest "App::CPAN", {
		plan 5;

		my $module = ModuleModel
			.new("App::CPAN");

		is $module.uuid, UUID, "UUID is empty";
		is $module.name, "App::CPAN", "Name is correct";
		is $module.version, Version, "Version is empty";
		is $module.api, Version, "API is empty";
		is $module.auth, Str, "Auth is empty";
	}

	subtest "App::CPAN:version<0.1.0+>", {
		plan 5;

		my $module = ModuleModel
			.new("App::CPAN:version<0.1.0+>");

		is $module.uuid, UUID, "UUID is empty";
		is $module.name, "App::CPAN", "Name is correct";
		is $module.version, "0.1.0+", "Version is correct";
		is $module.api, Version, "API is empty";
		is $module.auth, Str, "Auth is empty";
	}

	subtest "App::CPAN:version<0.1.0+>:api<*>", {
		plan 5;

		my $module = ModuleModel
			.new("App::CPAN:version<0.1.0+>:api<*>");

		is $module.uuid, UUID, "UUID is empty";
		is $module.name, "App::CPAN", "Name is correct";
		is $module.version, "0.1.0+", "Version is correct";
		is $module.api, "*", "API is correct";
		is $module.auth, Str, "Auth is empty";
	}

	subtest "App::CPAN:version<0.1.0+>:api<*>:auth<cpan:TYIL>", {
		plan 5;

		my $module = ModuleModel
			.new("App::CPAN:version<0.1.0+>:api<*>:auth<cpan:TYIL>");

		is $module.uuid, UUID, "UUID is empty";
		is $module.name, "App::CPAN", "Name is correct";
		is $module.version, "0.1.0+", "Version is correct";
		is $module.api, "*", "API is correct";
		is $module.auth, "cpan:TYIL", "Auth is correct";
	}
}

# vim: ft=perl6 noet
