#! /usr/bin/env perl6

use v6.d;

use App::CPAN::Util;
use Test;

plan 1;

subtest "split-author-field", {
	plan 4;

	subtest "Name only", {
		my %tests =
			"Patrick Spek" => { :name("Patrick Spek"), :email(''), :website('') },
			"John Doe" => { :name("John Doe"), :email(''), :website('') },
			"Jane Doe" => { :name("Jane Doe"), :email(''), :website('') },
		;

		plan %tests.elems;

		for %tests.kv -> $input, %output {
			is $input.&split-author-field, %output, $input;
		}
	}

	subtest "Name with email address", {
		my %tests =
			"Patrick Spek <p.spek@tyil.nl>" => { :name("Patrick Spek"), :email<p.spek@tyil.nl>, :website('') },
			"John Doe <john@example.com>" => { :name("John Doe"), :email<john@example.com>, :website('') },
			"Jane Doe <jane.doe@example.com>" => { :name("Jane Doe"), :email<jane.doe@example.com>, :website('') },
		;

		plan %tests.elems;

		for %tests.kv -> $input, %output {
			is $input.&split-author-field, %output, $input;
		}
	}

	subtest "Name with home page", {
		my %tests =
			"Patrick Spek (https://www.tyil.nl)" => { :name("Patrick Spek"), :email(''), :website<https://www.tyil.nl> },
			"John Doe (http://example.com)" => { :name("John Doe"), :email(''), :website<http://example.com> },
			"Jane Doe (https://example.com/~jane)" => { :name("Jane Doe"), :email(''), :website<https://example.com/~jane> },
		;

		plan %tests.elems;

		for %tests.kv -> $input, %output {
			is $input.&split-author-field, %output, $input;
		}
	}

	subtest "Name with email address and home page", {
		my %tests =
			"Patrick Spek <p.spek@tyil.nl> (https://www.tyil.nl)" => { :name("Patrick Spek"), :email<p.spek@tyil.nl>, :website<https://www.tyil.nl> },
			"John Doe <john@example.com> (http://example.com)" => { :name("John Doe"), :email<john@example.com>, :website<http://example.com> },
			"Jane Doe <jane.doe@example.com> (https://example.com/~jane)" => { :name("Jane Doe"), :email<jane.doe@example.com>, :website<https://example.com/~jane> },
		;

		plan %tests.elems;

		for %tests.kv -> $input, %output {
			is $input.&split-author-field, %output, $input;
		}
	}
}

# vim: ft=perl6 noet
